<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/6/2019
 * Time: 1:46 AM
 */
namespace Mageplaza\HelloWorld\Controller\Adminhtml\Category;

class Category extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Categories')));

        return $resultPage;
    }


}