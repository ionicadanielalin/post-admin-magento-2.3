<?php
/**
 * Mageplaza HelloWorld
 */
namespace Mageplaza\HelloWorld\Api;

use Mageplaza\HelloWorld\Api\Data\StoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Mageplaza\HelloWorld\Model\Post;

/**
 * Interface PostRepositoryInterface
 *
 * Mageplaza HelloWorld Post Repository Interface.
 * @package Mageplaza\HelloWorld\Api
 * @api
 */
interface PostRepositoryInterface
{
    /**
     * Save story.
     *
     * @param \Mageplaza\HelloWorld\Api\Data\PostInterface $post
     * @return \Mageplaza\HelloWorld\Api\Data\PostInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    function save(PostInterface $post);

    /**
     * Retrieve post.
     *
     * @param int $postId
     * @return \Mageplaza\HelloWorld\Api\Data\PostInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    function getById($postId);

    /**
     * Retrieve categories matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Mageplaza\HelloWorld\Api\Data\PostSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete post.
     *
     * @param \Mageplaza\HelloWorld\Api\Data\PostInterface $post
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    function delete(PostInterface $post);

    /**
     * Delete post by ID.
     *
     * @param int $postId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    function deleteById($postId);
}
