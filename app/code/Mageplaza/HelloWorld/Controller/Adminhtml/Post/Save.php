<?php
/**
 * Mageplaza Helloworld
 */
namespace Mageplaza\HelloWorld\Controller\Adminhtml\Post;

use Mageplaza\HelloWorld\Api\PostRepositoryInterface;
use Mageplaza\HelloWorld\Model\Post;
use Mageplaza\HelloWorld\Model\PostFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;

/**
 * Class Save
 *
 * @package Mageplaza\HelloWorld\Controller\Adminhtml\Post
 */
class Save extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Mageplaza_HelloWorld::post_save';

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var PostFactory
     */
    protected $postFactory;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param PostFactory $postFactory
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        StoryFactory $storyFactory,
        PostRepositoryInterface $storyRepository
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->dataPersistor = $dataPersistor;
        $this->storyFactory = $postFactory;
        $this->storyRepository = $postRepository;
    }

    /**
     * execute
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getParams();

        // get ID and prepare object
        $id = $this->getRequest()->getParam('post_id');

        // prepare status value
        if (
            array_key_exists('status', $data)
            && $data['status'] === true
        ) {
            $data['status'] = Post::STATUS_ENABLED;
        }

        try {
            if ($id) {
                // load the object
                $post = $this->postRepository->getById($id);
                if (!$post || !$post->getId()) {
                    $this->messageManager->addErrorMessage(__('This post no longer exists.'));
                    $resultRedirect = $this->resultRedirectFactory->create();
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                $data['post_id'] = null;
                $story = $this->postFactory->create();
            }

            $story->setData($data);

            // if thumbnail uploaded file is set, use the first one, as at this moment, only one thumbnail is allowed
            $post->setThumbnailPath(null);
            if (isset($data['thumbnail_path'][0]['name'])) {
                $post->setThumbnailPath($data['thumbnail_path'][0]['name']);
            }

            if (isset($data['thumbnail_path'][0]['file'])) {
                $post->setThumbnailPath($data['thumbnail_path'][0]['file']);
            }

            $this->storyRepository->save($post);
            $this->dataPersistor->clear('mageplaza_helloworld_post');

            // set success message
            $this->messageManager->addSuccessMessage(__('The post has been successfully saved.'));

            // if Save and Continue
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit', ['post_id' => $post->getId(), '_current' => true]);
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was an error when preparing the post.'));
            $resultRedirect = $this->resultRedirectFactory->create();
        }
        return $resultRedirect->setPath('*/*/');
    }
}
