<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/5/2019
 * Time: 9:57 PM
 */
namespace Mageplaza\HelloWorld\Model;
class Category extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'mageplaza_helloworld_category';

    protected $_cacheTag = 'mageplaza_helloworld_category';

    protected $_eventPrefix = 'mageplaza_helloworld_category';

    protected function _construct()
    {
        $this->_init('Mageplaza\HelloWorld\Model\ResourceModel\Category');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}