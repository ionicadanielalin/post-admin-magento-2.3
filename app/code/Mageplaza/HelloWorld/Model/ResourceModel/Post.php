<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/5/2019
 * Time: 10:00 PM
 */
namespace Mageplaza\HelloWorld\Model\ResourceModel;


class Post extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('mageplaza_helloworld_post', 'post_id');
    }

}