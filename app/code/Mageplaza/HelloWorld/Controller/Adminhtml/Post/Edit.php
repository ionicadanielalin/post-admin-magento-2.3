<?php
/**
 * Mageplaza HelloWorld
 */
namespace Mageplaza\HelloWorld\Controller\Adminhtml\Post;

use Mageplaza\HelloWorld\Api\PostRepositoryInterface;
use Mageplaza\HelloWorld\Model\Post;
use Mageplaza\HelloWorld\Model\PostFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 *
 *
 * @package Mageplaza\HelloWorld\Controller\Adminhtml\Posts
 */
class Edit extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Mageplaza_HelloWorld::post_save';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var StoryFactory
     */
    protected $postFactory;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * Edit constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $coreRegistry
     * @param PostFactory $storyFactory
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        PostFactory $postFactory,
        PostRepositoryInterface $postRepository
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->storyFactory = $postFactory;
        $this->storyRepository = $postRepository;
    }

    /**
     * execute
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mageplaza_Helloworld::content_news_post');
        $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
        $resultPage->addBreadcrumb(__('News'), __('News'));
        $resultPage->getConfig()->getTitle()->prepend(__('News Posts'));

        // get ID and prepare object
        $id = $this->getRequest()->getParam('post_id');
        try {
            if ($id) {
                // load the object
                $story = $this->storyRepository->getById($id);
                if (!$post || !$post->getId()) {
                    $this->messageManager->addErrorMessage(__('This post no longer exists.'));
                    $resultRedirect = $this->resultRedirectFactory->create();
                    return $resultRedirect->setPath('*/*/');
                }
                $resultPage->addBreadcrumb(__('Edit Post'), __('Edit Post'));
            } else {
                $story = $this->storyFactory->create();
                $resultPage->addBreadcrumb(__('Add Post'), __('Add Post'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was an error when preparing the post.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }

        // build edit form
        $this->coreRegistry->register('mageplaza_helloworld_post', $post);
        return $resultPage;
    }
}
