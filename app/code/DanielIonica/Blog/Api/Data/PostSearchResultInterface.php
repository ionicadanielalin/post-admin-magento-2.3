<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/11/2019
 * Time: 1:03 AM
 */
namespace DanielIonica\Blog\Api\Data;
use Magento\Framework\Api\SearchResultsInterface;
/**
 * Interface PostSearchResultInterface
 * @package DanielIonica\Blog\Api\Data
 */
interface PostSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \DanielIonica\Blog\Api\Data\PostInterface[]
     */
    public function getItems();
    /**
     * @param \DanielIonica\Blog\Api\Data\PostInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}