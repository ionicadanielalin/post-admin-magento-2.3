<?php
/**
 * Mageplaza HelloWorld
 */
namespace Mageplaza\HelloWorld\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface PostSearchResultsInterface
 *
 * Mageplaza HelloWorld Post Repository Interface.
 *
 * @package Mageplaza\HelloWorld\Api
 * @api
 */
interface PostSearchResultInterface extends SearchResultsInterface
{
    /**
     * Get categories list.
     * @return \Mageplaza\HelloWorld\Api\Data\PostInterface[]
     */
    function getItems();

    /**
     * Set categories list.
     * @param \Mageplaza\HelloWorld\Api\Data\StoryInterface[] $items
     * @return $this
     */
    function setItems(array $items);
}
