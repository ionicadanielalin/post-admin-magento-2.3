<?php
/**
 * Mageplaza HelloWorld
 */
namespace Mageplaza\HelloWorld\Controller\Adminhtml\Post;

use Mageplaza\HelloWorld\Api\PostRepositoryInterface;
use Mageplaza\HelloWorld\Block\Adminhtml\Post;
use Mageplaza\HelloWorld\Model\StoryFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 *
 * @package Mageplaza\HelloWorld\Controller\Adminhtml\Post
 */
class Delete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Mageplaza_HelloWorld::post_delete';

    /**
     * @var PostFactory
     */
    protected $postFactory;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(
        Context $context,
        PostFactory $storyFactory,
        PostRepositoryInterface $postRepository
    ) {
        parent::__construct($context);
        $this->postFactoryFactory = $postFactory;
        $this->postRepository = $postRepository;
    }

    /**
     * execute
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // get ID and prepare object
        $id = $this->getRequest()->getParam('post_id');
        if (!$id) {
            $this->messageManager->addErrorMessage(__('There was an error when processing the request.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }

        try {
            // load the object
            $story = $this->postRepository->getById($id);
            if (!$post || !$post->getId()) {
                $this->messageManager->addErrorMessage(__('This post no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }

            // delete the object
            $this->postRepository->delete($post);

            // set success message
            $this->messageManager->addSuccessMessage(__('The post has been successfully deleted.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was an error when preparing the post.'));
            $resultRedirect = $this->resultRedirectFactory->create();
        }
        return $resultRedirect->setPath('*/*/');
    }
}
