<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/11/2019
 * Time: 12:46 AM
 */
namespace DanielIonica\Blog\Api;
use DanielIonica\Blog\Api\Data\PostInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
/**
 * Interface PostRepositoryInterface
 * @package DanielIonica\Api
 * @api
 */
interface PostRepositoryInterface
{
    /**
     * @param int $id
     * @return \DanielIonica\Blog\Api\Data\PostInterface
     */
    public function get( $id);
    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \DanielIonica\Blog\Api\Data\PostSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
    /**
     * @param \DanielIonica\Blog\Api\Data\PostInterface $post
     * @return \DanielIonica\Blog\Api\Data\PostInterface
     */
    public function save(PostInterface $post);
    /**
     * @param \DanielIonica\Blog\Api\Data\PostInterface $post
     * @return bool
     */
    public function delete(PostInterface $post);
    /**
     * @param int $id
     * @return bool
     */
    public function deleteById( $id);
}