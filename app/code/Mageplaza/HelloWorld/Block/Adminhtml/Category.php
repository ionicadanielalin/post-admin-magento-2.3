<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/6/2019
 * Time: 1:42 AM
 */
namespace Mageplaza\HelloWorld\Block\Adminhtml;

class Category extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_controller = 'adminhtml_category';
        $this->_blockGroup = 'Mageplaza_HelloWorld';
        $this->_headerText = __('Categories');
        $this->_addButtonLabel = __('Create New Category');
        parent::_construct();
    }
}