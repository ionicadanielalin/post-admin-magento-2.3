<?php
/**
 * Mageplaza HelloWorld
 */
namespace Mageplaza\HelloWorld\Api\Data;

/**
 * Interface PostInterface
 *
 * @package Mageplaza\HelloWorld\Api
 * @api
 */
interface PostInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const POST_ID = 'post_id';
    const CATEGORY_ID = 'category_id';
    const TITLE = 'title';
    const URL_KEY = 'url_key';
    const POST_CONTENT = 'post_content';
    const TAGS = 'tags';
    const STATUS = 'status';
    const FEATURED_IMAGE = 'featured_image';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@- */

    /**
     * Get ID
     * @return int
     */
    function getId();

    /**
     * getTitle
     * @return mixed
     */
    function getTitle();

    /**
     * setTitle
     * @param string $title
     * @return $this|mixed
     */
    function setTitle($title);

    /**
     * getUrlKey
     * @return mixed|string
     */
    function getUrlKey();

    /**
     * setUrlKey
     * @param string $urlKey
     * @return $this|mixed
     */
    function setUrlKey($urlKey);

    /**
     * getPostContent
     * @return mixed|string
     */
    function getPostContent();

    /**
     * setPostContent
     * @param string $postContent
     * @return $this|mixed
     */
    function setContent($postContent);

    /**
     * getStatus
     * @return mixed|string
     */
    function getStatus();

    /**
     * setStatus
     * @param string $status
     * @return $this|mixed
     */
    function setStatus($status);

    /**
     * getTags
     * @return mixed|string
     */
    function getTags();

    /**
     * setTags
     * @param string $tags
     * @return $this|mixed
     */
    function setTags($tags);

    /**
     * getFeaturedImage
     * @return mixed|string
     */
    function getFeaturedImage();

    /**
     * setFeaturedImage
     * @param string $tags
     * @return $this|mixed
     */
    function setFeaturedImage($featuredImage);

    /**
     * getCategoryId
     * @return mixed
     */
    function getCategoryId();

    /**
     * setCategoryId
     * @param $categoryId
     * @return $this
     */
    function setCategoryId($categoryId);

    /**
     * Get created_at
     * @return string
     */
    function getCreatedAt();

    /**
     * Get updated_at
     * @return string
     */
    function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return mixed
     */
    function setUpdatedAt($updatedAt);
}
