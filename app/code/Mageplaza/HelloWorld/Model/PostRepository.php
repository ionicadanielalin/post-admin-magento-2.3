<?php
namespace Mageplaza\HelloWorld\Model;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\RequestInterface;
class PostRepository extends \Magento\Framework\Model\AbstractModel
{

protected $request;

public function __construct(RestRequest $request)
{

$this->request = $request;

}

public function getenquirydata($paramaters){

$post= $this->request->getPost();

// if you are trying to return $post over here then you need to encode post data into json like below :

$post = json_encode($this->request->getPost());

// if you are trying to return $paramaters; over here then you need to encode post data into json like below :

$paramaters = json_encode($this->request->getPost());
return $paramaters;

}
}