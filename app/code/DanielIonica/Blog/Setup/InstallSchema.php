<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 11/11/2019
 * Time: 12:50 AM
 */
namespace DanielIonica\Blog\Setup;
use DanielIonica\Blog\Api\Data\PostInterface;
use DanielIonica\Blog\Model\ResourceModel\Post as PostResource;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Security\Setup\InstallSchema as SecurityInstallSchema;
/**
 * Class InstallSchema
 * @package DanielIonica\Blog\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $table = $setup->getConnection()
            ->newTable(
                $setup->getTable(PostResource::TABLE_NAME)
            )
            ->addColumn(
                PostInterface::ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Post ID'
            )
            ->addColumn(
                PostInterface::AUTHOR_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => true,],
                'Author ID'
            )
            ->addColumn(
                PostInterface::TITLE,
                Table::TYPE_TEXT,
                255,
                [],
                'Title'
            )
            ->addColumn(
                PostInterface::CONTENT,
                Table::TYPE_TEXT,
                null,
                [],
                'Content'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Creation Time'
            )
            ->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Update Time'
            )
            ->addIndex(
                $setup->getIdxName(
                    PostResource::TABLE_NAME,
                    [PostInterface::AUTHOR_ID],
                    AdapterInterface::INDEX_TYPE_INDEX
                ),
                [PostInterface::AUTHOR_ID],
                ['type' => AdapterInterface::INDEX_TYPE_INDEX]
            )
            ->setComment('Posts')
        ;
        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }
}